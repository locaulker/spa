import React from "react"
import { Route, NavLink, Switch, Redirect } from "react-router-dom"
import Home from "./components/Home"
import About from "./components/About"
import Tutorials from "./components/Tutorials"
// import TDetails from "./components/TDetails"
import NoMatch from "./components/NoMatch"

function App() {
  return (
    <div className="App">
      <ul>
        <li>
          <NavLink to="/home">Home</NavLink>
        </li>
        <li>
          <NavLink to="/about">About</NavLink>
        </li>
        <li>
          <NavLink to="/tutorials">Tutorials</NavLink>
        </li>
        <hr />
      </ul>

      <Switch>
        <Route exact path="/">
          <Redirect to="/home" />
        </Route>
        <Route exact path="/">
          <Home />
        </Route>
        <Route path="/about">
          <About />
        </Route>
        <Route path="/tutorials">
          <Tutorials />
        </Route>
        <Route path="*">
          <NoMatch />
        </Route>
      </Switch>
    </div>
  )
}

export default App
