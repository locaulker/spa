import React from "react"
import { useParams, useHistory } from "react-router-dom"

const TDetails = () => {
  const params = useParams()
  const history = useHistory()

  console.log(history)

  return (
    <div>
      <h1>Tutorials Details - {params.title}</h1>
    </div>
  )
}

export default TDetails
