import React from "react"
import { Link } from "react-router-dom"

const Tutorials = () => {
  return (
    <div>
      <ul>
        <li>
          <Link to="/regex">Regular Expression</Link>
        </li>
        <li>
          <Link to="">Javascript Marathon Interview Questions Series</Link>
        </li>
        <li>
          <Link to="">Mastering Typescript + Interview Questions Series</Link>
        </li>
        <li>
          <Link to="">Mastering React17 + Interview Questions Series</Link>
        </li>
      </ul>
    </div>
  )
}

export default Tutorials
