import React from "react"
import { useLocation } from "react-router-dom"

const NoMatch = () => {
  const location = useLocation()
  console.log(location)

  return (
    <div>
      <h1>No Match {location.pathname}</h1>
    </div>
  )
}

export default NoMatch
